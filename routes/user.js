const express = require("express");
const router = express.Router();

const User = require("../models/User.js");

const userController = require("../controllers/userController.js")

const auth = require("../auth.js")




router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)); 
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/details", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})






module.exports = router;

