const Course = require("../models/Course");
const User = require("../models/User");

module.exports.addCourse = (reqBody) => {


	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.descriptions,
		price: reqBody.price
	})

	return User.findOne({isAdmin : true}).then(result => {
		if(result !== null){
			newCourse.save().then((newCourse, error) => {
				if (error){
					return false
				}

				return true
			})
		}

		else{
				return false
			}
		})

}